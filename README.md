Python CFFI bindings for xxHash library
=======================================

This code wraps the [xxhash](https://code.google.com/p/xxhash/) hash library for python using [cffi](http://cffi.readthedocs.org/en/release-0.8/).

> xxHash is an extremely fast non-cryptographic Hash algorithm, working at speeds close to RAM limits. 

This wrapper was tested with `xxHash-r39`.

Compiling xxhash as a shared lib
================================
To use xxhash with python/cffi, it must be compiled as a shared library.

1. Download [xxHash-r39 src](https://github.com/Cyan4973/xxHash/releases/latest) from github
2. Copy `xxhash.c` and `xxhash.h` into this directory.
3. Type `make`

This has only been tested on OSX and only with **r39** of the xxHash.

Example
=======
* See `example_hash.py` to see it in action.

Tests
=====
* run tests with `py.test`
