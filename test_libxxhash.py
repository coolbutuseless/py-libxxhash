#!/usr/bin/env python
from __future__ import print_function

from libxxhash import libxxhash

def test_simple():
    # The test hashes were calculated using `xxhsum` at the command line.
    # e.g. `echo "the quick brown fox jumps over the lazy dog" | ./xxHash-r39/xxhsum -`
    # Note the lack of "\n" at the command line
    tests = [
        (b"the quick brown fox jumps over the lazy dog\n", 0x1662c91a, 0x4d32a62b7b695236),
        (b"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n", 0x05d011323, 0x23f6b6246e9c7939),
        (b"Nam tincidunt congue enim, ut porta lorem lacinia consectetur.\n", 0x0eab1443, 0x61e468069624e707)
    ]
    for teststring, hash32_ref, hash64_ref in tests:
        hash32 = libxxhash.XXH32(teststring, len(teststring), 0) 
        hash64 = libxxhash.XXH64(teststring, len(teststring), 0) 
        print(teststring, (hash32_ref, hash64_ref), (hash32, hash64))
        assert hash32 == hash32_ref, "Failure on:" + teststring
        assert hash64 == hash64_ref
