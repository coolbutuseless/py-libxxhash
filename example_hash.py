#!/usr/bin/env python
from __future__ import print_function

from libxxhash import libxxhash

seed = 0

#============================================================
# Simple one-shot hash
#============================================================
teststring = b"hello there"
hash64 = libxxhash.XXH64(teststring, len(teststring), seed) 
print("64bit hash of a single teststring: ", teststring, hash64)


#============================================================
# Hash with update
#============================================================
teststringa = b"hello "
teststringb = b"there"

# Create the xxhash state ready to be used
xxhash_state = libxxhash.XXH64_createState()
err = libxxhash.XXH64_reset(xxhash_state, seed)
assert err==0

# Hash the first string
err = libxxhash.XXH64_update(xxhash_state, teststringa, len(teststringa))
assert err==0

# Has the second string
err = libxxhash.XXH64_update(xxhash_state, teststringb, len(teststringb))
assert err==0

# retrieve the current state of the hash
res = libxxhash.XXH64_digest(xxhash_state)
print("64bit hash of multiple strings", [teststringa, teststringb], res)
print("Equals hash of total string?", res == libxxhash.XXH64(teststringa+teststringb, len(teststringa+teststringb), seed))

# Free the state
err = libxxhash.XXH64_freeState(xxhash_state)
assert err==0

