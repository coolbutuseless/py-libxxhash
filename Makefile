all: xxhash.o
	gcc -shared -o libxxhash.so.1.0 xxhash.o

OPTS := -I. -std=c99 -Ofast -Wall -Wextra -Wundef -Wshadow -Wstrict-prototypes -fPIC

xxhash.o: xxhash.c 
	gcc $(OPTS) -c xxhash.c

clean:
	rm *.o *.so.*
