#!/usr/bin/env python
from __future__ import print_function

from cffi import FFI
ffi = FFI()

with open("libxxhash.h") as header:
    ffi.cdef(header.read())

# To create the xxhash library, simply copy the
# file `xxhash.c` to this directory and type `make`
libxxhash = ffi.dlopen("libxxhash.so.1.0")
